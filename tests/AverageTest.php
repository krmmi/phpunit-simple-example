<?php

use drmonkeyninja\Average;
use PHPUnit\Framework\TestCase;
/**
 * @covers drmonkeyninja\Average
 */
class AverageTest extends TestCase
{
    protected $Average;

    public function setUp() :void
    {
        $this->Average = new Average();
    }
/**
 * @covers drmonkeyninja\Average::mean()
 */
    public function testCalculationOfMean() :void
    {
        $numbers = [3, 7, 6, 1, 5];
        $this->assertEquals(4.4, $this->Average->mean($numbers));
    }
/**
 * @covers drmonkeyninja\Average::median()
 */
    public function testCalculationOfMedian() :void
    {
        $numbers = [3, 7, 6, 1, 5];
        $this->assertEquals(5, $this->Average->median($numbers));
    }
    /**
     * @covers drmonkeyninja\Average::median()
     */
    public function test2CalculationOfMedian() :void
    {
        $numbers = [3, 7, 6, 1];
        $this->assertEquals(4.5, $this->Average->median($numbers));
    }




}

